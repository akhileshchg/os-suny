/*
 * Copyright (c) 1998-2011 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2011 Stony Brook University
 * Copyright (c) 2003-2011 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef _WRAPFS_H_
#define _WRAPFS_H_

#include <linux/dcache.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/seq_file.h>
#include <linux/statfs.h>
#include <linux/fs_stack.h>
#include <linux/magic.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
/* the file system name */
#define WRAPFS_NAME "u2fs"

/* wrapfs root inode number */
#define WRAPFS_ROOT_INO     1

/* useful for tracking code reachability */
#define UDBG printk(KERN_DEFAULT "DBG:%s:%s:%d\n", __FILE__, __func__, __LINE__)
/*Copy up functions*/
#include "sioq.h"

extern int copyup_dentry(struct inode *dir, struct dentry *dentry, const char *name,
                        int namelen, struct file **copyup_file, loff_t len);
struct dentry *create_parents(struct inode *dir, struct dentry *dentry,
                              const char *name, int bindex);
void wrapfs_reinterpose(struct dentry *dentry);
int copyup_file(struct inode *dir, struct file *file, loff_t len);
void wrapfs_postcopyup_setmnt(struct dentry *dentry);
void wrapfs_postcopyup_release(struct dentry *dentry);
void release_lower_nd(struct nameidata *nd, int err);
int init_lower_nd(struct nameidata *nd, unsigned int flags);
/* operations vectors defined in specific files */
extern const struct file_operations wrapfs_main_fops;
extern const struct file_operations wrapfs_dir_fops;
extern const struct inode_operations wrapfs_main_iops;
extern const struct inode_operations wrapfs_dir_iops;
extern const struct inode_operations wrapfs_symlink_iops;
extern const struct super_operations wrapfs_sops;
extern const struct dentry_operations wrapfs_dops;
extern const struct address_space_operations wrapfs_aops, wrapfs_dummy_aops;
extern const struct vm_operations_struct wrapfs_vm_ops;

extern int wrapfs_init_inode_cache(void);
extern void wrapfs_destroy_inode_cache(void);
extern int wrapfs_init_dentry_cache(void);
extern void wrapfs_destroy_dentry_cache(void);
extern int new_dentry_private_data(struct dentry *dentry);
extern void free_dentry_private_data(struct dentry *dentry);
extern struct dentry *wrapfs_lookup(struct inode *dir, struct dentry *dentry,
				    struct nameidata *nd);
extern struct inode *wrapfs_iget(struct super_block *sb,
				 struct inode **lower_inode, int count);
extern int wrapfs_interpose(struct dentry *dentry, struct super_block *sb,
			    struct path *lower_path, int index);

/* file private data */
struct wrapfs_file_info {
	int count;
	struct file **lower_file;
	const struct vm_operations_struct *lower_vm_ops;
};

/* wrapfs inode data in memory */
struct wrapfs_inode_info {
	int count;
	struct inode **lower_inode;
	struct inode vfs_inode;
};

/* wrapfs dentry data in memory */
struct wrapfs_dentry_info {
	spinlock_t lock;	/* protects lower_path */
	atomic_t count;
	struct path *lower_path;
};

/* wrapfs super-block data in memory */
struct wrapfs_sb_info {
	int count;
	spinlock_t slock;
	struct super_block *lower_sb[2];
};

/*
 * inode to private data
 *
 * Since we use containers and the struct inode is _inside_ the
 * wrapfs_inode_info structure, WRAPFS_I will always (given a non-NULL
 * inode pointer), return a valid non-NULL pointer.
 */
static inline struct wrapfs_inode_info *WRAPFS_I(const struct inode *inode)
{
	return container_of(inode, struct wrapfs_inode_info, vfs_inode);
}

/* dentry to private data */
#define WRAPFS_D(dent) ((struct wrapfs_dentry_info *)(dent)->d_fsdata)

/* superblock to private data */
#define WRAPFS_SB(super) ((struct wrapfs_sb_info *)(super)->s_fs_info)

/* file to private Data */
#define WRAPFS_F(file) ((struct wrapfs_file_info *)((file)->private_data))

/* file to lower file */
static inline struct file *wrapfs_lower_file(const struct file *f, int index)
{
	//BUG_ON(!f || index < 0 || index >= 2 );
	return WRAPFS_F(f)->lower_file[index];
}

static inline void wrapfs_set_lower_file(struct file *f, struct file *val, int  index)
{
	//BUG_ON(!f || index < 0 || index >=2);
	WRAPFS_F(f)->lower_file[index] = val;
}

/* inode to lower inode. */
static inline struct inode *wrapfs_lower_inode(const struct inode *i, int index)
{
	//BUG_ON(!i || index < 0 || index >=2);
	return WRAPFS_I(i)->lower_inode[index];
}

static inline void wrapfs_set_lower_inode(struct inode *i, struct inode *val, int index)
{
	//BUG_ON(!i || index < 0 || index >=2);
	WRAPFS_I(i)->lower_inode[index] = val;
}

/* superblock to lower superblock */
static inline struct super_block *wrapfs_lower_super(
	const struct super_block *sb, int index)
{
	//BUG_ON(!sb || index < 0 || index >= 2);
	return WRAPFS_SB(sb)->lower_sb[index];
}

static inline void wrapfs_set_lower_super(struct super_block *sb,
				struct super_block *val, int index)
{
	//BUG_ON(!sb || index < 0 || index >=2);
	WRAPFS_SB(sb)->lower_sb[index] = val;
}

/* path based (dentry/mnt) macros */
static inline void pathcpy(struct path *dst, const struct path *src)
{
	dst->dentry = src->dentry;
	dst->mnt = src->mnt;
}
/* Returns struct path.  Caller must path_put it. */
static inline void wrapfs_get_lower_path(const struct dentry *dent,
					 struct path *lower_path, int index)
{
	spin_lock(&WRAPFS_D(dent)->lock);
	pathcpy(lower_path, &WRAPFS_D(dent)->lower_path[index]);
	path_get(lower_path);
	spin_unlock(&WRAPFS_D(dent)->lock);
	return;
}
static inline struct vfsmount *wrapfs_lower_mnt_idx(
                    const struct dentry *dent,
                    int index)
{
    BUG_ON(!dent || index < 0);
    return WRAPFS_D(dent)->lower_path[index].mnt;
}

static inline struct vfsmount *wrapfs_mntget(struct dentry *dentry,
                          int bindex)
{
    struct vfsmount *mnt;

    BUG_ON(!dentry || bindex < 0);

    mnt = mntget(wrapfs_lower_mnt_idx(dentry, bindex));

    return mnt;
}
static inline void wrapfs_set_lower_mnt_idx(struct dentry *dent, int index,
                         struct vfsmount *mnt)
{
    BUG_ON(!dent || index < 0);
    WRAPFS_D(dent)->lower_path[index].mnt = mnt;
}

static inline struct dentry *wrapfs_lower_dentry(
		const struct dentry *dent,
		int index)
{
    BUG_ON(!dent || index < 0);
    return WRAPFS_D(dent)->lower_path[index].dentry;
}
static inline void wrapfs_set_lower_dentry(struct dentry *dent,
			int index, struct dentry *val)
{
    BUG_ON(!dent || index < 0);
    WRAPFS_D(dent)->lower_path[index].dentry = val;
}


static inline void wrapfs_put_lower_path(const struct dentry *dent,
					 struct path *lower_path)
{
	path_put(lower_path);
	return;
}
static inline void wrapfs_set_lower_path(const struct dentry *dent,
					 struct path *lower_path, int index)
{	//BUG_ON(!dent || index < 0 || index >= 2);
	spin_lock(&WRAPFS_D(dent)->lock);
	pathcpy(&WRAPFS_D(dent)->lower_path[index], lower_path);
	spin_unlock(&WRAPFS_D(dent)->lock);
	return;
}
static inline void wrapfs_reset_lower_path(const struct dentry *dent, int index)
{
	spin_lock(&WRAPFS_D(dent)->lock);
	WRAPFS_D(dent)->lower_path[index].dentry = NULL;
	WRAPFS_D(dent)->lower_path[index].mnt = NULL;
	spin_unlock(&WRAPFS_D(dent)->lock);
	return;
}
static inline void wrapfs_put_reset_lower_path(const struct dentry *dent, int index)
{
	struct path lower_path;
	spin_lock(&WRAPFS_D(dent)->lock);
	pathcpy(&lower_path, &WRAPFS_D(dent)->lower_path[index]);
	WRAPFS_D(dent)->lower_path[index].dentry = NULL;
	WRAPFS_D(dent)->lower_path[index].mnt = NULL;
	spin_unlock(&WRAPFS_D(dent)->lock);
	path_put(&lower_path);
	return;
}

/* locking helpers */
static inline struct dentry *lock_parent(struct dentry *dentry)
{
	struct dentry *dir = dget_parent(dentry);
	mutex_lock_nested(&dir->d_inode->i_mutex, I_MUTEX_PARENT);
	return dir;
}

static inline void unlock_dir(struct dentry *dir)
{
	mutex_unlock(&dir->d_inode->i_mutex);
	dput(dir);
}
static inline struct dentry *lookup_lck_len(const char *name,
                        struct dentry *base, int len)
{
    struct dentry *d;

    mutex_lock(&base->d_inode->i_mutex);
    d = lookup_one_len(name, base, len); // XXX: pass flags?
    mutex_unlock(&base->d_inode->i_mutex);

    return d;
}
static inline void path_put_lowers(struct dentry *dentry,
                   int bstart, int bend, bool free_lower)
{
    struct dentry *lower_dentry;
    struct vfsmount *lower_mnt;
    int bindex;

    BUG_ON(!dentry);
    BUG_ON(!WRAPFS_D(dentry));
    BUG_ON(bstart < 0);

    for (bindex = bstart; bindex <= bend; bindex++) {
        lower_dentry = wrapfs_lower_dentry(dentry, bindex);
        if (lower_dentry) {
            wrapfs_set_lower_dentry(dentry, bindex, NULL);
            dput(lower_dentry);
        }
        lower_mnt = wrapfs_lower_mnt_idx(dentry, bindex);
        if (lower_mnt) {
            wrapfs_set_lower_mnt_idx(dentry, bindex, NULL);
            mntput(lower_mnt);
        }
    }

    if (free_lower) {
        kfree(WRAPFS_D(dentry)->lower_path);
        WRAPFS_D(dentry)->lower_path = NULL;
    }
}
static inline void iput_lowers(struct inode *inode,
                   int bstart, int bend, bool free_lower)
{
    struct inode *lower_inode;
    int bindex;

    BUG_ON(!inode);
    BUG_ON(!WRAPFS_I(inode));
    BUG_ON(bstart < 0);

    for (bindex = bstart; bindex <= bend; bindex++) {
        lower_inode = wrapfs_lower_inode(inode, bindex);
        if (lower_inode) {
            wrapfs_set_lower_inode(inode, NULL, bindex);
            /* see Documentation/filesystems/wrapfs/issues.txt */
            lockdep_off();
            iput(lower_inode);
            lockdep_on();
        }
    }

    if (free_lower) {
        kfree(WRAPFS_I(inode)->lower_inode);
        WRAPFS_I(inode)->lower_inode = NULL;
    }
}
static inline int d_deleted(struct dentry *d)
{
	return d_unhashed(d) && (d != d->d_sb->s_root);
}

#endif	/* not _WRAPFS_H_ */
