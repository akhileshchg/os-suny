xhw1:

NAME 
	xhw1 -	Concatenates list of input files to an output files.

SYNOPSIS
	./xhw1 [flags] outfile infile1 infile2 ...


DESCRIPTION:
	An interface to take list of input files, special flags and an output file.
	Concatenates the input files into out based on special flags. Internally Uses xconcat system call.
	Could return number of bytes written or any error.
	
	./xhw1 [-acte] [-m default_mode] [-NPA] outfile infile1 infile2 ...

	-c	Create outfile if it does not exist.

	-a	Appends the concatenated data to outfile if it already exists. 
		Fails if the outfile does not exist.

	-t	Truncates outfile before writing concatenated data to outfile.
		Fails if the outfile does not exist.
	
	-m	The octal mode in which the outfile should be opened. 
		Default value is rwx for the user(700) creating the outfile.
		Does nothing when outfile is already present. On any invalid value result would be similar to output of
		strol for base 8.
	
	-N	returns number of files instead of number of number of bytes written.
	-P	returns percentage of bytes total bytes written instead of number of bytes written.
	-A	Does not return  any partial writes. Returns total number of bytes written if the concatenation is successful.
		Else, returns appropriate error and reverts the outfile to previous state.

	1. Used getopt example on www.gnu.org to process commandline arguments

----------------------------------------------------------------------------------------------------------------------------------
xconcat:

NAME
	xconcat - concatenates list of input files and writes to output file.

SYNOPSIS
	xconcat (void* arg, int size);

ARGUMENTS
	arg	:	Arugments packed under one void *pointer
		Contains
      		
		outfile : output file to which concat result is written to
		nfiles : Array of input files
		infile_count : Number of input files
		oflags  : Openflags for output file. Similar to open system call
		mode    : Open mode for output file. Similar to open system call
      		flags   : Modifies out result on setting this variable.
                	Valid values are 0, 1, 2, and 4.
			0:	On success,returns num of bytes written to out
				On failure, could return error or num of bytes
				written till then
			1:	Same as 0. But returns number of files written
				instead ofnumber of bytes written.
			2:	Same as 0. But returns percentage of total
				bytes written instead of number of bytes writes
			4:	Atomic mode: returns number of bytes written on
				sucess.Reverts output file to original state
				state and returns appropriate error code.
	size 	:	Size of the memory referenced by arg


DESCRIPTION
===========
The xconcat function starts is written in three phases.
1. Initialization
=================
Checks if user memory reference passed is valid and checks for valid validility of memory size.
Checks for valid input file count and returns EINVAL for invalid numbers and E2BIG for more than 10 input files.
Checks for validity of input and output files, flags and modes. Returns appropriate error on failure.
Checks for validity of special return flags. returns EINVAL when 0,1,2,4 or combinations with atomic (2|4, 1|4) is not passed.
Checks if all the files can be opened. Else, clean up is followed and appropriate error is returned.
Checks if output file matches with any of the input files, returns EINVAL when matched.
Automic Mode:
-------------
On flag 4, creates temp file and copies contents of output when O_TRUNC is not given.
Makes sure, permissions uid, gid are same for both output file and temp file.
On failure returns appropriate error.

2. Concatenation
================
Reads every file in loop (for each read PAGESIZE data is copied) and writes to buffer.
On error during read or write, returns number of bytes (number of files, percentage of total bytes) written till then.
On paritial write from write(2) call, rewrites are performed indefinitely till entire buffer is written.
Automic Mode:
-------------
On error during read or write, automic clean up is followed and returns appropriate error.

3. Clean Up
===========
Cleans up all the dynamic memory allocated till then.
Closes all the opened files.
Automic Mode
------------
If the concat is successful, output file is unlinked and temp file is renamed to output file.
If the concat fails, temp file is unlinked. 
If the concat fails and the output file is created for the for time, then output is also unlinked.

RESOURCES USED
==============
1. www.gnu.org (for getopt usage)
2. Linux Kernel Source Code and Documentation, http://lxr.free-electrons.com/source/ 
3. Daniel P. Bovet, Marco Cesati. Understanding the linux kernel, Third Edition

