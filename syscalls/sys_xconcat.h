#ifndef XCONCAT
#define XCONCAT

/*
 *Special flags to change return values.
 *Induvidual flag must be set or combinations of C_ATMIC 
 *with other induvidual flags can be set.
 */
#define C_BYTES 0
#define C_FILES 1
#define C_PCENT 2
#define C_ATMIC	4

struct ip {
	__user const char *outfile;
	__user const char **infiles;
	unsigned int infile_count;
	int oflags;
	mode_t mode;
	unsigned int flags;
};
/*
 *openfile -    Opens file in kernel space.
 *		on failure, returns appropriate error and sets @out arg to NULL
 *@out		: The file pointer address which references the opened file
 *@filename     : The name of the file to be opened.
 *@flags	: The flags used to open file. Refer to flags in open(2) call.
 *@mode		: The mode used to open file. Refer to mode in open(2) call.
 */
int openfile(struct file **out, const char *filename, int flags, int mode)
{
	int ret = 0;
	*out = filp_open(filename, flags, mode);
	if (!(*out) || IS_ERR(*out)) {
		ret = PTR_ERR(*out);
		*out = NULL;
	}
	return ret;
}
/*
 *Closefile -   Closes file in kernel space.
 *@out		: File pointer address which references the file to be closed.
 */
void closefile(struct file **out)
{
	if (*out)
		filp_close(*out, NULL);
	*out = NULL;
}
/*
 *readfile -    Reads @size bytes from open file into buffer
 *		Returns appropriate error on failure.
 *@fd		: The open file reference from which data is read.
 *@buf		: The buffer into which the data is copied
 *@size		: The amount of bytes that must be read
 */
int readfile(struct file **fd, char *buf, int size)
{
	int bytes  = -EBADF;
	if (*fd) {
		mm_segment_t oldfs;
		oldfs = get_fs();
		set_fs(KERNEL_DS);
		bytes = vfs_read(*fd, buf, size, &(*fd)->f_pos);
		set_fs(oldfs);
	}
	return bytes;
}
/*
 *writefile -	Writes @size bytes to open file from buffer
 *		Returns appropriate error on failure.
 *@fd		: The open file reference to which data is written.
 *@buf		: The buffer from which the data is written
 *@size		: The amount of bytes that must be written
 */
int writefile(struct file **fd, char *buf, int size)
{
	int bytes = -EBADF;
	if (*fd) {
		mm_segment_t oldfs;
		oldfs = get_fs();
		set_fs(KERNEL_DS);
		bytes = vfs_write(*fd, buf, size, &(*fd)->f_pos);
		set_fs(oldfs);
	}
	return bytes;
}
/*
 *swap_file - swaps two file references.
 */
inline void swap_file(struct file **a, struct file **b)
{
	struct file *tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

#endif
